import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';

@Module({
  imports: [
    ProductsModule,
    MongooseModule.forRoot(
      'mongodb+srv://app-admin:Apside!23091994@cluster0.frjxpbw.mongodb.net/nestjs-app?retryWrites=true&w=majority',
    ),
  ],
  controllers: [AppController],
  providers: [AppService, ProductsModule],
})
export class AppModule {}
