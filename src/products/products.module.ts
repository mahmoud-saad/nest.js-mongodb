import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProdcutsController } from './prodcuts.controller';
import { ProductsService } from './products.service';
import { ProductSchema } from './product.model';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema }]),
  ],
  controllers: [ProdcutsController],
  providers: [ProductsService],
})
export class ProductsModule {}
